#!/bin/bash

# “strict mode”
set +m -euo pipefail
IFS=$'\n\t '

restic \
    forget \
        --keep-yearly 5 \
        --keep-monthly 12 \
        --keep-weekly 13 # 1/4y

restic \
    prune
