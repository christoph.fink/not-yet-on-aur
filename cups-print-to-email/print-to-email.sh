#!/bin/bash

# “strict mode”
set +m -euo pipefail
IFS=$'\n\t '

declare DESTINATION_EMAIL
declare JOB_NAME
declare PDF
declare REMOTE_USER
declare TEMPORARY_DIRECTORY


if [[ "$#" -eq "0" ]]; then
    # backend discovery: answer something meaningful
    echo 'network print-to-email "Generic" "print to email address"'
    exit 0
fi

# https://stackoverflow.com/questions/296536/how-to-urlencode-data-for-curl-command
rawurldecode() {
  printf -v REPLY '%b' "${1//%/\\x}" # You can either set a return variable (FASTER)
  echo "${REPLY}"  #+or echo the result (EASIER)... or both... :p
}

JOB_NAME="${3//\//-}"
REMOTE_USER="$2"
DESTINATION_EMAIL="$(rawurldecode "$(echo "${DEVICE_URI}" | sed 's/^print-to-email:\/*//')")"

TEMPORARY_DIRECTORY="$(sudo -u "${REMOTE_USER}" -- mktemp -d)"
chgrp cups "${TEMPORARY_DIRECTORY}"
chmod 0770 "${TEMPORARY_DIRECTORY}"

PDF="${TEMPORARY_DIRECTORY}/${JOB_NAME}.pdf"

# if called with 6 arguments, the 6th is the input file
# else stdin
# see https://www.cups.org/doc/api-filter.html#OVERVIEW
if [[ "$#" -eq "6" ]]; then
    cp "$6" "${PDF}"
else
    cat > "${PDF}"
fi
chown "${REMOTE_USER}" "${PDF}"

echo "${JOB_NAME}" \
    | sudo \
        -u "${REMOTE_USER}" \
        -- \
        mailx \
            -s "${JOB_NAME}" \
            -a "${PDF}" \
            "${DESTINATION_EMAIL}"

rm -R "${TEMPORARY_DIRECTORY}"

